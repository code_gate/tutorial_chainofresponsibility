﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tutorial_chainofresponsibility.ChainImplementation
{
    class SuperPrize : Handler
    {
        public SuperPrize()
        {
            SetSuccessor = new NoPrize();
        }

        public SuperPrize(Handler handler)
        {
            SetSuccessor = handler;
        }

        public override void HandleRequest(int lotteryNumber)
        {
            if (lotteryNumber >= 80 && lotteryNumber < 100) 
            {
                Console.WriteLine($"You've won {this.GetType().Name}");
            }
            else
            {
                Successor.HandleRequest(lotteryNumber);
            }
        }
    }
}
