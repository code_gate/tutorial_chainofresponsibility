﻿using System;

namespace tutorial_chainofresponsibility.ChainImplementation
{
    class NoPrize : Handler
    {
        public override void HandleRequest(int lotteryNumber)
        {
            Console.WriteLine("You've lost. Try again");
        }
    }
}
