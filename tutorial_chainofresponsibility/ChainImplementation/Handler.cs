﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tutorial_chainofresponsibility.ChainImplementation
{
    public abstract class Handler
    {
        protected Handler Successor;

        public abstract void HandleRequest(int lotteryNumber);

        protected Handler SetSuccessor
        {
            set => Successor = value;
        }
    }
}
