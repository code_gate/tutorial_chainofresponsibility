﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tutorial_chainofresponsibility.ChainImplementation
{
    class GrandPrize : Handler
    {
        public GrandPrize()
        {
            SetSuccessor = new NoPrize();
        }

        public GrandPrize(Handler handler)
        {
            SetSuccessor = handler;
        }

        public override void HandleRequest(int lotteryNumber)
        {
            if (lotteryNumber == 100) 
            {
                Console.WriteLine($"You've won {this.GetType().Name}");
            }
            else
            {
                Successor.HandleRequest(lotteryNumber);
            }
        }
    }
}
