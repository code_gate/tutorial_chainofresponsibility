﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tutorial_chainofresponsibility.ChainImplementation
{
    class StandardPrize : Handler
    {
        public StandardPrize()
        {
            SetSuccessor = new NoPrize();
        }

        public StandardPrize(Handler handler)
        {
            SetSuccessor = handler;
        }

        public override void HandleRequest(int lotteryNumber)
        {
            if (lotteryNumber < 80) 
            {
                Console.WriteLine($"You've won {this.GetType().Name}");
            }
            else
            {
                Successor.HandleRequest(lotteryNumber);
            }
        }
    }
}
