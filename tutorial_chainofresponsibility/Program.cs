﻿using System;
using System.Collections.Generic;
using tutorial_chainofresponsibility.ChainImplementation;

namespace tutorial_chainofresponsibility
{
    class Program
    {
        static void Main(string[] args)
        {

            var lotteryNumbers = new List<int>() { 50, 80, 100, 200 };

            var lotteryPrizeChain = new StandardPrize(new SuperPrize(new GrandPrize()));

            lotteryNumbers.ForEach((item) => lotteryPrizeChain.HandleRequest(item));

            Console.ReadLine();
        }
    }
}
